package kr.co.kinetic27.translate

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import java.io.BufferedReader
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder


/**
* Created by Kinetic on 2018-07-29.
*/

@Suppress("EXPERIMENTAL_FEATURE_WARNING")
class MainActivity : BaseActivity() {


    override var viewId: Int = R.layout.activity_main
    override var toolbarId: Int? = R.id.toolbar

    private val clientId = "여기에"
    private val clientSecret = "넣으셈"

    override fun onCreate() {
        copy_btn.setOnClickListener {
            (getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager).primaryClip = ClipData.newPlainText("아희로", result_keyboard.text)
            Toast.makeText(this, "클립보드에 복사되었습니다!", Toast.LENGTH_SHORT).show()
        }

        hint_keyboard.textChanged {
            when {
                it.isNotEmpty() -> launch(UI) {
                    result_keyboard.text = getString(R.string.loading_text)
                    val firstText = translateText(hint_keyboard.text.toString(), "en", "ja")
                    result_keyboard.text = translateText(firstText!!, "ja", "ko")
                }
                else -> result_keyboard.text = getString(R.string.loading_text)
            }
        }
    }

    private fun EditText.textChanged(onTextChanged: (String) -> Unit) {

        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                onTextChanged.invoke(s.toString())
            }

            override fun afterTextChanged(editable: Editable?) {
            }
        })

    }

    private suspend fun translateText(sourceText: String, sourceLang: String, targetLang: String): String? {

        val task = async taskAsync@ {
            try {
                val text = URLEncoder.encode(sourceText, "UTF-8")
                val apiUrl = URL("https://openapi.naver.com/v1/papago/n2mt")
                val con = (apiUrl.openConnection() as HttpURLConnection).apply {
                    requestMethod = "POST"
                    setRequestProperty("X-Naver-Client-Id", clientId)
                    setRequestProperty("X-Naver-Client-Secret", clientSecret)
                    doOutput = true
                }

                DataOutputStream(con.outputStream).apply {
                    writeBytes("source=$sourceLang&target=$targetLang&text=$text")
                    flush()
                    close()
                }

                val br = when (con.responseCode) {
                    200 -> BufferedReader(InputStreamReader(con.inputStream))
                    else -> BufferedReader(InputStreamReader(con.errorStream))
                }

                val response = StringBuffer()

                while (true) {
                    val line = br.readLine() ?: break
                    response.append(line)
                }

                br.close()
                val rootObj = JsonParser().parse(response.toString())
                        .asJsonObject.get("message")
                        .asJsonObject.get("result")
                val item = GsonBuilder().create().fromJson<TranslatedItem>(rootObj.toString(), TranslatedItem::class.java)
                Log.e("", "err : ${item.translatedText}")
                return@taskAsync item.translatedText
            } catch (e: Exception) {
                Log.e("", "err : ${e.message}")
                return@taskAsync e.message
            }
        }

        return task.await()
    }

    private inner class TranslatedItem {
        var translatedText: String? = null
            internal set
    }
}
